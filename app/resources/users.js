'use strict';

var Joi = require('joi');

const Users = module.exports = { }

Users.registerRoutes = function (server, logger) {
    const 
        harvesterPlugin = server.plugins['hapi-harvester'],
        schema = {
            type: 'users',
            attributes: {
                name: Joi.string().max(200).required(),
                email: Joi.string().email().required(),
                avatar: Joi.string().uri(),
                city: Joi.string(),
                subdivision: Joi.string(),
                country: Joi.string(),                
            },
            relationships: {
                stats: { data: { type: "userStats" } }
            }
        }

    harvesterPlugin.routes.all(schema).forEach(function (route) {
        server.route(route);
    })
}